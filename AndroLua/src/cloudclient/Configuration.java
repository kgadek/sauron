package cloudclient;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import cloudclient.net.ApiConfiguration;

/**
 * Created by banan on 19.01.14.
 */
public class Configuration {

    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mPreferenceEditor;
    private static final String PREFS_API_URL = "apiUrl";

    public Configuration(Context ctx) {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        mPreferenceEditor = mSharedPreferences.edit();
    }

    public String getApiUrl() {
        return mSharedPreferences.getString(PREFS_API_URL, ApiConfiguration.API_BASE);
    }

    public void setApiUrl(String url) {
        mPreferenceEditor.putString(PREFS_API_URL, url);
        mPreferenceEditor.commit();
    }
}
