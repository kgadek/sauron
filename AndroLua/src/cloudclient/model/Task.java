package cloudclient.model;

import java.io.Serializable;

/**
 * Created by banan on 13.01.14.
 */
public class Task implements Serializable {

    private Long id;
    private String code;

    public Task() { ; }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
