package cloudclient.model;

/**
 * Created by banan on 13.01.14.
 */
public class SpecialTask {

    private Long id;
    private Long type;
    private Double lat;
    private Double lon;
    private Double rad;
    private Long deadline;
    private String name;
    private String description;

    public SpecialTask() {;}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getRad() {
        return rad;
    }

    public void setRad(Double rad) {
        this.rad = rad;
    }

    public Long getDeadline() {
        return deadline;
    }

    public void setDeadline(Long deadline) {
        this.deadline = deadline;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
