package cloudclient;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import cloudclient.model.SpecialTask;
import sk.kottman.androlua.R;

/**
 * Created by banan on 16.01.14.
 */
public class TaskAdapter extends BaseAdapter{

    private List<SpecialTask> mTasks;

    private Location mCurrentLocation;
    private Context mContext;

    public TaskAdapter(Context ctx, List<SpecialTask> tasks) {
        super();
        mTasks = tasks;
        mContext = ctx;
    }

    public void setCurrentLocation(Location mCurrentLocation) {
        this.mCurrentLocation = mCurrentLocation;
        notifyDataSetInvalidated();
    }

    @Override
    public int getCount() {
        return mTasks.size();
    }

    @Override
    public Object getItem(int i) {
        return mTasks.get(i);
    }

    @Override
    public long getItemId(int i) {
        return mTasks.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.task_list_item, null);
        }
        SpecialTask task = mTasks.get(i);
        ((TextView)view.findViewById(R.id.textView)).setText(task.getName());
        ((TextView)view.findViewById(R.id.textView2)).setText(task.getDescription());
        if (task.getRad() == -1) {
            ((TextView)view.findViewById(R.id.textView3)).setVisibility(View.GONE);
        } else {
            ((TextView)view.findViewById(R.id.textView3)).setVisibility(View.VISIBLE);
            if (mCurrentLocation!=null) {
                ((TextView)view.findViewById(R.id.textView3)).setText(task.getDescription());
                Location loc = new Location("");
                loc.setLatitude(task.getLat());
                loc.setLongitude(task.getLon());
                float dist = mCurrentLocation.distanceTo(loc);
                ((TextView)view.findViewById(R.id.textView3)).setText("Odległość od punktu: " + dist);
                if (dist > task.getRad()) {
                    ((TextView)view.findViewById(R.id.textView3)).setTextColor(Color.RED);
                } else {
                    ((TextView)view.findViewById(R.id.textView3)).setTextColor(Color.GREEN);
                }
            } else {
                ((TextView)view.findViewById(R.id.textView3)).setText("Nie można określić lokalizacji");
                ((TextView)view.findViewById(R.id.textView3)).setTextColor(Color.RED);
            }
        }
        return view;
    }

    public boolean isActive(int i) {
        SpecialTask task = mTasks.get(i);
        if (task.getRad() == -1)
            return true;
        Location loc = new Location("");
        loc.setLatitude(task.getLat());
        loc.setLongitude(task.getLon());
        float dist = mCurrentLocation.distanceTo(loc);
        return dist<task.getRad();
    }
}
