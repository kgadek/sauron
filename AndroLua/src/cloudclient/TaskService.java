package cloudclient;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import org.keplerproject.luajava.JavaFunction;
import org.keplerproject.luajava.LuaException;
import org.keplerproject.luajava.LuaState;
import org.keplerproject.luajava.LuaStateFactory;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

import cloudclient.model.Task;
import cloudclient.net.RequestManager;
import sk.kottman.androlua.R;

/**
 * Created by banan on 13.01.14.
 */
public class TaskService extends Service {

    public static final String ARG_TASK = "argTask";
    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;

    private Task task;


    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {

            final Task t = (Task)msg.getData().getSerializable(ARG_TASK);

            Log.d("SERVICE", t.getCode());

            serverThread = new ServerThread();
            serverThread.start();
            L = LuaStateFactory.newLuaState();
            L.openLibs();

            try {
                L.pushJavaObject(TaskService.this);
                L.setGlobal("android");

                JavaFunction print = new JavaFunction(L) {
                    @Override
                    public int execute() throws LuaException {
                        for (int i = 2; i <= L.getTop(); i++) {
                            int type = L.type(i);
                            String stype = L.typeName(type);
                            String val = null;
                            if (stype.equals("userdata")) {
                                Object obj = L.toJavaObject(i);
                                if (obj != null)
                                    val = obj.toString();
                            } else if (stype.equals("boolean")) {
                                val = L.toBoolean(i) ? "true" : "false";
                            } else {
                                val = L.toString(i);
                            }
                            if (val == null)
                                val = stype;
                            output.append(val);
                            output.append("\t");
                        }
                        output.append("\n");
//                        Log.d("resss", "send:" + output.toString());
//                        Toast.makeText(TaskService.this, "will send: " + output, Toast.LENGTH_SHORT).show();
//                        /**
//                         * SEND REPLY
//                         */
//                        try {
//                            boolean sent = new RequestManager().executeResult(t.getId(),output.toString(),bestLocation);
//                            if (sent)
//                                Log.d("resss", "send:" + output.toString());
////                                Toast.makeText(TaskService.this, "sent result", Toast.LENGTH_SHORT).show();
//                            else
////                                Toast.makeText(TaskService.this, "error in sending result", Toast.LENGTH_SHORT).show();
//                                Log.d("resss", "send fail:" + output.toString());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        } finally {
//                            output.delete(0, output.length()-1);
//                        }
                        return 0;
                    }
                };
                print.register("print");

                JavaFunction assetLoader = new JavaFunction(L) {
                    @Override
                    public int execute() throws LuaException {
                        String name = L.toString(-1);

                        AssetManager am = getAssets();
                        try {
                            InputStream is = am.open(name + ".lua");
                            byte[] bytes = readAll(is);
                            L.LloadBuffer(bytes, name);
                            return 1;
                        } catch (Exception e) {
                            ByteArrayOutputStream os = new ByteArrayOutputStream();
                            e.printStackTrace(new PrintStream(os));
                            L.pushString("Cannot load module "+name+":\n"+os.toString());
                            return 1;
                        }
                    }
                };

                L.getGlobal("package");            // package
                L.getField(-1, "loaders");         // package loaders
                int nLoaders = L.objLen(-1);       // package loaders

                L.pushJavaFunction(assetLoader);   // package loaders loader
                L.rawSetI(-2, nLoaders + 1);       // package loaders
                L.pop(1);                          // package

                L.getField(-1, "path");            // package path
                String customPath = getFilesDir() + "/?.lua";
                L.pushString(";" + customPath);    // package path custom
                L.concat(2);                       // package pathCustom
                L.setField(-2, "path");            // package
                L.pop(1);
            } catch (Exception e) {
//                status.setText("Cannot override print");
            }

            try {
                String res = evalLua(t.getCode());
//                status.append(res);
                Log.d("SERVICE RESULT", res);

//                Toast.makeText(TaskService.this, "will send: " + res, Toast.LENGTH_SHORT).show();
//                /**
//                 * SEND REPLY
//                 */
//                try {
//                    boolean sent = new RequestManager().executeResult(t.getId(),res,bestLocation);
//                    if (sent)
//                        Toast.makeText(TaskService.this, "sent result", Toast.LENGTH_SHORT).show();
//                    else
//                        Toast.makeText(TaskService.this, "error in sending result", Toast.LENGTH_SHORT).show();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
            } catch(LuaException e) {
                Toast.makeText(TaskService.this, e.getMessage(), Toast.LENGTH_LONG).show();
                Log.e("LUA error", e.getMessage()  + " " + e.getLocalizedMessage());
            }

            Log.d("service", "service stopped");
            serverThread.stopped = true;
            stopSelf(msg.arg1);
        }
    }

    public IBinder onBind(Intent intent) {


        return null;
    }

    @Override
    public void onCreate() {
        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.

        initializeLocationManager();
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
            bestLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (bestLocation == null)
                bestLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        } catch (java.lang.SecurityException ex) {
            Log.d("service_loc", "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d("service_loc", "network provider does not exist, " + ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            Log.d("service_loc", "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d("service_loc", "gps provider does not exist " + ex.getMessage());
        }

        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    private Task t;
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        int icon = R.drawable.icon;
        CharSequence notiText = "Cloud Client - computing";
        long meow = System.currentTimeMillis();

        Notification notification = new Notification(icon, notiText, meow);

        Context context = getApplicationContext();
        CharSequence contentTitle = "CloudClient";
        CharSequence contentText = "Computing..";
        Intent notificationIntent = new Intent();
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);

        int SERVER_DATA_RECEIVED = 324324;
        notificationManager.notify(SERVER_DATA_RECEIVED, notification);

        task = (Task)intent.getExtras().getSerializable(ARG_TASK);
        t = task;
        if (task != null) {
            // For each start request, send a message to start a job and deliver the
            // start ID so we know which request we're stopping when we finish the job
            Message msg = mServiceHandler.obtainMessage();
            msg.arg1 = startId;
            Bundle b = new Bundle();
            b.putSerializable(ARG_TASK, task);
            msg.setData(b);
            new ServiceHandler(getMainLooper()).sendMessage(msg);
            // If we get killed, after returning from here, restart
        }
        return START_STICKY;
    }


    @Override
    public void onDestroy() {

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        int SERVER_DATA_RECEIVED = 324324;
        notificationManager.cancel(SERVER_DATA_RECEIVED);

        Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.d("service_loc", "fail to remove location listners, ignore", ex);
                }
            }
        }
    }


    ///-----------
    private final static int LISTEN_PORT = 3333;


    // public so we can play with these from Lua
    public LuaState L;

    final StringBuilder output = new StringBuilder();

    Handler handler;
    ServerThread serverThread;

    private static byte[] readAll(InputStream input) throws Exception {
        ByteArrayOutputStream output = new ByteArrayOutputStream(4096);
        byte[] buffer = new byte[4096];
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
        }
        return output.toByteArray();
    }


    private class ServerThread extends Thread {
        public boolean stopped;

        @Override
        public void run() {
            stopped = false;
            try {
                ServerSocket server = new ServerSocket(LISTEN_PORT);
                show("Server started on port " + LISTEN_PORT);
                while (!stopped) {
                    Socket client = server.accept();
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(client.getInputStream()));
                    final PrintWriter out = new PrintWriter(client.getOutputStream());
                    String line = null;
                    while (!stopped && (line = in.readLine()) != null) {
                        final String s = line.replace('\001', '\n');
                        if (s.startsWith("--mod:")) {
                            int i1 = s.indexOf(':'), i2 = s.indexOf('\n');
                            String mod = s.substring(i1+1,i2);
                            String file = getFilesDir()+"/"+mod.replace('.', '/')+".lua";
                            FileWriter fw = new FileWriter(file);
                            fw.write(s);
                            fw.close();
                            // package.loaded[mod] = nil
                            L.getGlobal("package");
                            L.getField(-1, "loaded");
                            L.pushNil();
                            L.setField(-2, mod);
                            out.println("wrote " + file + "\n");
                            out.flush();
                        } else {
                            handler.post(new Runnable() {
                                public void run() {
                                    String res = safeEvalLua(s);
                                    res = res.replace('\n', '\001');
                                    out.println(res);
                                    out.flush();
                                }
                            });
                        }
                    }
                }
                server.close();
            } catch (Exception e) {
                show(e.toString());
            }
        }

        private void show(final String s) {
        }
    }

    String safeEvalLua(String src) {
        String res = null;
        try {
            res = evalLua(src);
        } catch(LuaException e) {
            res = e.getMessage()+"\n";
        }
        return res;
    }

    String evalLua(String src) throws LuaException {
        L.setTop(0);
        int ok = L.LloadString(src);
        if (ok == 0) {
            L.getGlobal("debug");
            L.getField(-1, "traceback");
            L.remove(-2);
            L.insert(-2);
            ok = L.pcall(0, 0, -2);
            if (ok == 0) {
                String res = output.toString();
                output.setLength(0);
                return res;
            }
        }
        throw new LuaException(errorReason(ok) + ": " + L.toString(-1));
        //return null;

    }


    private String errorReason(int error) {
        switch (error) {
            case 4:
                return "Out of memory";
            case 3:
                return "Syntax error";
            case 2:
                return "Runtime error";
            case 1:
                return "Yield error";
        }
        return "Unknown error " + error;
    }

    // LOCATION
    private Location bestLocation = null;
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 10f;
    private class LocationListener implements android.location.LocationListener{
        Location mLastLocation;
        public LocationListener(String provider)
        {
            Log.d("service_location", "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }
        @Override
        public void onLocationChanged(Location location)
        {
            Log.d("service_location", "onLocationChanged: " + location);
            mLastLocation.set(location);
            if (isBetterLocation(mLastLocation, bestLocation))
                bestLocation = mLastLocation;
        }
        @Override
        public void onProviderDisabled(String provider)
        {
            Log.d("service_location", "onProviderDisabled: " + provider);
        }
        @Override
        public void onProviderEnabled(String provider)
        {
            Log.d("service_location", "onProviderEnabled: " + provider);
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras)
        {
            Log.d("service_location", "onStatusChanged: " + provider);
        }
    }
    LocationListener[] mLocationListeners = new LocationListener[] {
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };
    private void initializeLocationManager() {
        Log.d("service_loc", "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    private static final int TWO_MINUTES = 1000 * 60 * 2;

    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    // LUA METHODS
    public void sleep(long millis) {
        SystemClock.sleep(millis);
    }

    public String getLatitude() {
        return String.valueOf(bestLocation.getLatitude());
    }

    public String getLongitude() {
        return String.valueOf(bestLocation.getLongitude());
    }

    public Long getTime() {
        return new Date().getTime();
    }

    public void send(String send) {
        Log.d("resss", "send:" + send);
        Toast.makeText(TaskService.this, "will send: " + send, Toast.LENGTH_SHORT).show();
        /**
         * SEND REPLY
         */
        try {
            boolean sent = new RequestManager().executeResult(this, t.getId(),send,bestLocation);
            if (sent)
                Log.d("resss", "send:" + send);
//                                Toast.makeText(TaskService.this, "sent result", Toast.LENGTH_SHORT).show();
            else
//                                Toast.makeText(TaskService.this, "error in sending result", Toast.LENGTH_SHORT).show();
                Log.d("resss", "send fail:" +send);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
