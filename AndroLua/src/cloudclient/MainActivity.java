package cloudclient;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cloudclient.net.ApiConfiguration;
import cloudclient.net.RequestManager;
import cloudclient.net.request.GetTasksRequest;
import cloudclient.net.response.TasksResponse;
import sk.kottman.androlua.R;

/**
 * Created by banan on 13.01.14.
 */
public class MainActivity extends Activity {

    private ListView mList;
    private TextView mEmpty;
    private TaskAdapter mAdapter;
    private EditText mUrlEdit;

    private static final int CAMERA_REQUEST = 123;
    private static final String CAMERA_ARG_ID = "cameraId";

    private static long taskId = -1l;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeLocationManager();
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
            bestLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (bestLocation == null)
                bestLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        } catch (java.lang.SecurityException ex) {
            Log.d("service_loc", "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d("service_loc", "network provider does not exist, " + ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            Log.d("service_loc", "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d("service_loc", "gps provider does not exist " + ex.getMessage());
        }
        mList = (ListView)findViewById(R.id.list);
        mEmpty = (TextView)findViewById(R.id.empty);
        mUrlEdit = (EditText)findViewById(R.id.url);

        mUrlEdit.setText(ApiConfiguration.API_BASE);
        mUrlEdit.setText(new Configuration(this).getApiUrl());
        ((Button)findViewById(R.id.button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Configuration(MainActivity.this).setApiUrl(mUrlEdit.getText().toString());
                RequestManager rm = new RequestManager();
                try {
                    String response = rm.executeTasksRequest(MainActivity.this, getRequest());
                    Log.d("tasks", response);
                    TasksResponse resp = new TasksResponse();
                    Gson gson = new Gson();
                    resp = gson.fromJson(response, TasksResponse.class);

                    if (resp.getBackgroundTasks().size() > 0) {
                        Intent intent = new Intent(MainActivity.this,
                                TaskService.class);
                        intent.putExtra(TaskService.ARG_TASK, resp.getBackgroundTasks().get(0));
                        startService(intent);
                    }

                    if (resp.getSpecialTasks().size() > 0) {
                        mList.setVisibility(View.VISIBLE);
                        mEmpty.setVisibility(View.GONE);

                        mAdapter = new TaskAdapter(MainActivity.this, resp.getSpecialTasks());
                        if (bestLocation != null)
                            mAdapter.setCurrentLocation(bestLocation);
                        mList.setAdapter(mAdapter);

                        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                if (mAdapter.isActive(i)) {
                                    taskId = mAdapter.getItemId(i);
                                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                    cameraIntent.putExtra(CAMERA_ARG_ID, mAdapter.getItemId(i));
                                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                                } else {
                                    Toast.makeText(MainActivity.this, "Nie możesz wykonać tego zadania w tej lokalizacji",
                                            Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                    } else {
                        mList.setVisibility(View.GONE);
                        mEmpty.setVisibility(View.VISIBLE);
                    }

//                    if (resp.getBackgroundTasks().size() > 1) {
//                        Intent intent = new Intent(MainActivity.this,
//                                TaskBgService.class);
//                        intent.putExtra(TaskBgService.ARG_TASK, resp.getBackgroundTasks().get(1));
//                        startService(intent);
//                    }

                } catch (IOException e) {
                    Toast.makeText(MainActivity.this, "Nie udało się wysłać zgłoszenia",
                            Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.d("service_loc", "fail to remove location listners, ignore", ex);
                }
            }
        }
    }

    public GetTasksRequest getRequest() {
        GetTasksRequest request = new GetTasksRequest();
        request.setSensors(getAvailableSensors());
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        if (bestLocation == null)
            bestLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (bestLocation == null)
            bestLocation = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (bestLocation == null)
            bestLocation = lm.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        if (bestLocation != null) {
            request.setLat(bestLocation.getLatitude());
            request.setLon(bestLocation.getLatitude());
        } else {
            request.setLon(-1.);
            request.setLon(-1.);
        }
        return request;
    }

    public List<Integer> getAvailableSensors() {
        List<Integer> result = new ArrayList<Integer>();
        LocationManager lm = null;
        boolean gps_enabled = false,network_enabled = false;
        if(lm==null)
            lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try{
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }catch(Exception ex){}
        try{
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }catch(Exception ex){}
        if (network_enabled || gps_enabled)
            result.add(0);
        SensorManager sensorManager
                = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> listSensor
                = sensorManager.getSensorList(Sensor.TYPE_ALL);
        for(int i=0; i<listSensor.size(); i++)
           result.add(listSensor.get(i).getType());
        return result;
    }


    // LOCATION
    private Location bestLocation = null;
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 10f;
    private class LocationListener implements android.location.LocationListener{
        Location mLastLocation;
        public LocationListener(String provider)
        {
            Log.d("service_location", "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }
        @Override
        public void onLocationChanged(Location location)
        {
            Log.d("service_location", "onLocationChanged: " + location);
            mLastLocation.set(location);
            if (isBetterLocation(mLastLocation, bestLocation)) {
                bestLocation = mLastLocation;
                if (mAdapter != null) {
                    mAdapter.setCurrentLocation(bestLocation);
                }
            }
        }
        @Override
        public void onProviderDisabled(String provider)
        {
            Log.d("service_location", "onProviderDisabled: " + provider);
        }
        @Override
        public void onProviderEnabled(String provider)
        {
            Log.d("service_location", "onProviderEnabled: " + provider);
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras)
        {
            Log.d("service_location", "onStatusChanged: " + provider);
        }
    }
    LocationListener[] mLocationListeners = new LocationListener[] {
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };
    private void initializeLocationManager() {
        Log.d("service_loc", "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    private static final int TWO_MINUTES = 1000 * 60 * 2;

    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }
        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
//            ((ImageView)findViewById(R.id.img)).setImageBitmap(photo);
//            Long id
            new SenderTask(taskId).execute(photo);
        }
    }

    class SenderTask extends AsyncTask<Bitmap, Void, Boolean> {

        private ProgressDialog mProgress;
        private Long id;

        public SenderTask(Long id) {
            super();
            this.id = id;
        }

        @Override
        protected void onPreExecute() {
            mProgress = new ProgressDialog(MainActivity.this);
            mProgress.setMessage("Proszę czekać.");
            mProgress.show();
        }

        @Override
        protected Boolean doInBackground(Bitmap... bitmaps) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmaps[0].compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            String base = Base64.encodeToString(byteArray, Base64.URL_SAFE);
            Log.d("base", base);
            try {
                return new RequestManager().executeResult(MainActivity.this,id, base,bestLocation);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            mProgress.dismiss();
            if (result) {
                Toast.makeText(MainActivity.this, "Wysłano", Toast.LENGTH_LONG).show();
            } else {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Błąd")
                        .setMessage("Nie udało się przesłać pliku")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).show();
            }
        }
    }
}