package cloudclient.net;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

import cloudclient.Configuration;
import cloudclient.net.request.GetTasksRequest;

public class RequestManager {
	
	private HttpClient httpClient;

	/**
	 * initialize httpClient if not exists, if exists just returns it
	 * @return initialized HttpClient
	 * @throws org.apache.http.client.ClientProtocolException
	 * @throws java.io.IOException
	 */
	private HttpClient getHttpClient() throws ClientProtocolException, IOException {
		if (httpClient != null)
			return httpClient;
		HttpParams httpParameters = new BasicHttpParams();
		int timeoutConnection = 10000;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		int timeoutSocket = 15000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        httpClient = new DefaultHttpClient(httpParameters);
        return httpClient;
	}

	/**
	 * method executes given query and returns server response in plain text
	 *
	 * @param query
	 * @return server reply as String
	 * @throws org.apache.http.client.ClientProtocolException
	 * @throws java.io.IOException
	 */
	private  String execute(String query, String sensors, String lat, String lon) throws ClientProtocolException, IOException {

//        return "{" +
//                "background_tasks:[" +
//                    "{id:1," +
//                    "code:\"" +
//                "log = luajava.bindClass(\\\"android.util.Log\\\")\n" +
//                "logt = getmetatable(log).__index\n" +
//                "logt(log, 'i')(log, 'MyApp', \\\"Hello World!\\\");\n" +
//                "print(\\\"TESTOWYYY\\\")\"" +
//                    "}" +
//                "], special_tasks:[]}";



        //TODO
		BufferedReader in = null;
		String result;
		try {
			Log.d(getClass().getSimpleName(), "url: " + query);
			HttpClient httpClient = getHttpClient();
            HttpPost httpPost = new HttpPost(query);

//            List<NameValuePair> params = new LinkedList<NameValuePair>();
//
//            params.add(new BasicNameValuePair("sensors", sensors));
//            params.add(new BasicNameValuePair("lat", lat));
//            params.add(new BasicNameValuePair("lon", lon));
//
//            httpPost.setEntity(new UrlEncodedFormEntity(params));

            StringBuilder sb = new StringBuilder();
            sb.append("{");
            sb.append("\"lat\":");
            sb.append(lat);
            sb.append(",");
            sb.append("\"lon\":");
            sb.append(lon);
            sb.append(",");
            sb.append("\"sensors\":[");
            sb.append(sensors.substring(0,sensors.length()-1));
            sb.append("]}");

            httpPost.setEntity(new StringEntity(sb.toString()));

            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            Log.d("param", sb.toString());

			HttpResponse response = httpClient.execute(httpPost);
			if(response == null){
				throw new IOException();
			}
			in = new BufferedReader (new InputStreamReader(response.getEntity().getContent()));
			StringBuffer sb2 = new StringBuffer("");
			String line = "";
			while ((line = in.readLine()) != null) {
				sb2.append(line);
			}
			result = sb2.toString();
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e2) {
					e2.printStackTrace();
				}
		}
		Log.d(getClass().getSimpleName(), "Server replied: " + result);
		return result;
	}

	/**
	 * method executes given query and returns server response InputStreamReader
	 *
	 * @param query
	 * @return server reply as String
	 * @throws org.apache.http.client.ClientProtocolException
	 * @throws java.io.IOException
	 */
	private  InputStreamReader executeISR(String query) throws ClientProtocolException, IOException {
		Log.d(getClass().getSimpleName(), "url: " + query);
		HttpClient httpClient = getHttpClient();
		HttpGet httpGet = new HttpGet(query);

		HttpResponse response = httpClient.execute(httpGet);
		if(response == null){
			throw new IOException();
		}
		return new InputStreamReader(response.getEntity().getContent());
	}
	
	public String executeTasksRequest(Context ctx, GetTasksRequest request) throws ClientProtocolException, IOException {
        StringBuilder sb = new StringBuilder();
        for (Integer s : request.getSensors()) {
            sb.append(String.valueOf(s+","));
        }
		return execute(new Configuration(ctx).getApiUrl() + "/tasks", sb.toString().substring(0,sb.length()-2),
                String.valueOf(request.getLat()),
                String.valueOf(request.getLon()));
	}


    public  boolean executeResult(Context ctx, Long id, String content, Location loc) throws ClientProtocolException, IOException {

        BufferedReader in = null;
        String result;
            String query = new Configuration(ctx).getApiUrl() + "/tasks/result";

            Log.d(getClass().getSimpleName(), "url: " + query);
            HttpClient httpClient = getHttpClient();
            HttpPost httpPost = new HttpPost(query);

//            List<NameValuePair> params = new LinkedList<NameValuePair>();
//
//            params.add(new BasicNameValuePair("id", String.valueOf(id)));
//            params.add(new BasicNameValuePair("time",  String.valueOf(new Date().getTime())));
//            params.add(new BasicNameValuePair("lat",  String.valueOf(loc.getLatitude())));
//            params.add(new BasicNameValuePair("lat",  String.valueOf(loc.getLatitude())));
//            params.add(new BasicNameValuePair("result",  content));

//            httpPost.setEntity(new UrlEncodedFormEntity(params));
            StringBuilder sb = new StringBuilder();
            sb.append("{");
            sb.append("\"lat\":");
            sb.append(loc.getLatitude());
            sb.append(",");
            sb.append("\"lon\":");
            sb.append(loc.getLongitude());
            sb.append(",");
            sb.append("\"id\":");
            sb.append(id);
            sb.append(",");
            sb.append("\"time\":");
            sb.append(new Date().getTime());
            sb.append(",");
            sb.append("\"result\":\"");
            sb.append(content.replaceAll("\n", ""));
            sb.append("\"}");


        httpPost.setEntity(new StringEntity(sb.toString()));

        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");

        Log.d("param", sb.toString());

            HttpResponse response = httpClient.execute(httpPost);
            if(response == null){
                return false;
            }
            if (response.getStatusLine().getStatusCode() == 200) {
                return true;
            }
            return false;
    }

    public boolean executePhoto(Long id, String answer, Location loc) throws ClientProtocolException, IOException {

        return false;
    }
}
