package cloudclient.net.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import cloudclient.model.SpecialTask;
import cloudclient.model.Task;

/**
 * Created by banan on 13.01.14.
 */
public class TasksResponse {

    @SerializedName("background_tasks")
    private List<Task> backgroundTasks;

    @SerializedName("special_tasks")
    private List<SpecialTask> specialTasks;

    public TasksResponse() {;}

    public List<Task> getBackgroundTasks() {
        return backgroundTasks;
    }

    public void setBackgroundTasks(List<Task> backgroundTasks) {
        this.backgroundTasks = backgroundTasks;
    }

    public List<SpecialTask> getSpecialTasks() {
        return specialTasks;
    }

    public void setSpecialTasks(List<SpecialTask> specialTasks) {
        this.specialTasks = specialTasks;
    }
}
