package cloudclient.net;

/**
 * Created by adam on 05.08.13.
 */
public class ApiConfiguration {

	public static final String API_BASE = "http://hip.hypersquare.com/adam_test";
    public static final String GET_URL =  "/tasks";
    public static final String RESULT_URL = "/result";

}
