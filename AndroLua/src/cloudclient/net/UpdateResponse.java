package cloudclient.net;

import java.util.ArrayList;
import java.util.List;

public class UpdateResponse {

	private String table;
	private List<String> structure;
	private List<List<String>> data;

	public List<List<String>> getData() {
		return data;
	}

	public List<String> getStructure() {
		return structure;
	}

	public String getTable() {
		return table;
	}

	public void setData(List<List<String>> data) {
		this.data = data;
	}

	public void setStructure(List<String> structure) {
		this.structure = structure;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public List<String> toSql() {
		
		List<String> result = new ArrayList<String>();
		
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO " + table + "(");
		for (String name : structure) {
			sb.append(name + ", ");
		}
		sb.setLength(sb.length()-2);
		sb.append(") VALUES (");

		for (List<String> vals : data) {
			StringBuilder values = new StringBuilder();
			for (String value : vals) {
				if (value == null)
					values.append("null" + ", ");
				else
					values.append("\"" + value.replaceAll("\"", "&quot;") + "\", ");
			}
			values.setLength(values.length()-2);
			values.append(");\n");
			result.add(sb.toString() + values);
		}
		return result;
	}

}
