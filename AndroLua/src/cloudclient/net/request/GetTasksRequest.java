package cloudclient.net.request;

import java.util.List;

/**
 * Created by banan on 13.01.14.
 */
public class GetTasksRequest {

    private double lat;
    private double lon;
    private List<Integer> sensors;

    public GetTasksRequest() {;};

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public List<Integer> getSensors() {
        return sensors;
    }

    public void setSensors(List<Integer> sensors) {
        this.sensors = sensors;
    }
}
