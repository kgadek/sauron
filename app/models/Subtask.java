package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.databind.node.ObjectNode;

import play.db.ebean.Model;
import play.libs.Json;

@Entity
public class Subtask extends Model{
  private static final long serialVersionUID = 7291205029016428503L;
  public boolean resolved;
  public boolean isMerge;
  public String val1;
  public String val2;
  @Column(length=10000)
  public String code;
  @Id
  public long subtaskId;
  @ManyToOne(optional=false)
  @Column(name="task_id")
  private Task task;
  
  public Subtask(boolean isMerge, String val1, String val2, String code, Task task) {
    super();
    this.isMerge = isMerge;
    this.val1 = val1;
    this.val2 = val2;
    this.code = code;
    this.task = task;
  }

  public ObjectNode asJson() {
    ObjectNode taskNode = Json.newObject();
    taskNode.put("code", fixCodeWithInput());
    taskNode.put("id", task.id);
    taskNode.put("subtask_id", subtaskId);
    return taskNode;
  }
  
  public void setVal2(String val2) {
    this.val2 = val2;
    update();
  }
  
  private String fixCodeWithInput() {
    String code = this.code;
    String temp; 
    if(isMerge) {
      temp = val2;
      code = "local val2 = " + temp.replaceAll("\\[", "\\{").replaceAll("\\]", "\\}") + "\n" + code;
	}
	temp = val1;
	code = "local val1 = " + temp.replaceAll("\\[", "\\{").replaceAll("\\]", "\\}") + "\n" + code;
    return code;
  }
}
