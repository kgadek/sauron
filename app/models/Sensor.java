package models;

import java.util.LinkedHashMap;
import java.util.Map;


public enum Sensor {
  TYPE_GPS(0),
  TYPE_ACCELEROMETER(1),
  TYPE_MAGNETIC_FIELD(2),
  TYPE_ORIENTATION(3),
  TYPE_GYROSCOPE(4),
  TYPE_LIGHT(5),
  TYPE_PRESSURE(6),

  TYPE_TEMPERATURE(7),
  TYPE_PROXIMITY(8),
  TYPE_GRAVITY(9),
  TYPE_LINEAR_ACCELERATION(10),
  TYPE_ROTATION_VECTOR(11),
  TYPE_RELATIVE_HUMIDITY(12),
  TYPE_AMBIENT_TEMPERATURE(13),
  TYPE_MAGNETIC_FIELD_UNCALIBRATED(14),
  TYPE_GAME_ROTATION_VECTOR(15),
  TYPE_GYROSCOPE_UNCALIBRATED(16),
  TYPE_SIGNIFICANT_MOTION(17),
  TYPE_STEP_DETECTOR(18),
  TYPE_STEP_COUNTER(19),
  TYPE_GEOMAGNETIC_ROTATION_VECTOR(20);
  
  public final int id;
  
  private Sensor(int id) {
    this.id = id;
  }
  
  public static Sensor fromId(String id) {
    return Sensor.values()[Integer.parseInt(id)];
  }
  
  public static Map<String, String> options() {
    LinkedHashMap<String, String> options = new LinkedHashMap<String, String>();
    for (Sensor sensor : Sensor.values()) {
        options.put(sensor.id+"",sensor.toString());
    }
    return options;
  }
}
