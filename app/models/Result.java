package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import play.db.ebean.Model;

@Entity
public class Result extends Model {
  private static final long serialVersionUID = 647543868069587165L;
  @Id
  public long id;
  @Column(length=100000,name="RESULT")
  public String result;
  public double resultLon;
  public double resultLat;
  public String resultTime;
  @ManyToOne(optional=false)
  @Column(name="task_id")
  private Task task;

  public Result(String result, double resultLon, double resultLat, String resultTime, Task task) {
    this.result = result;
    this.resultLon = resultLon;
    this.resultLat = resultLat;
    this.resultTime = resultTime;
    this.task = task;
    save();
  }
}
