package models;

import java.util.LinkedHashMap;
import java.util.Map;
import play.Logger;

public enum TaskType {
  BACKGROUND_TASK(0) {

    public boolean isBackgroundTask() {
      return true;
    }
  },
  PHOTO(1),
  MAP_REDUCE(2);

  public final int id;

  private TaskType(int id) {
    this.id = id;
  }

  public static TaskType fromId(String id) {
    return TaskType.values()[Integer.parseInt(id)];
  }

  public boolean isBackgroundTask() {
    return false;
  }
  
  public static Map<String, String> options() {
    LinkedHashMap<String, String> options = new LinkedHashMap<String, String>();
    for (TaskType taskType : TaskType.values()) {
        options.put(taskType.id+"",taskType.toString());
		
    }
    return options;
	}
	
	public static Map<String, String> oneOption(String oneString) {
    LinkedHashMap<String, String> options = new LinkedHashMap<String, String>();
		for (TaskType taskType : TaskType.values()) 
		{
			if(oneString.equals(taskType.id+"")){
					options.put(taskType.id+"",taskType.toString());
			}
		}
		return options;
	}
}
