package models;

import java.util.LinkedHashMap;
import java.util.Map;


public enum VerificationStrategy {
  NO_VERIFICATION(1, "No verification") {
    @Override
    public String toString() {
      return "No verification";
    }
  },
  VERIFICATION_BY_CODE(2, "Verification by code"){
    @Override
    public String toString() {
      return "Verification by code";
    }
  },
  VERIFICATION_BY_MAJORITY(3, "Verification by majority"){
    @Override
    public String toString() {
      return "Verification by majority";
    }
  };

  public final int id;
  public final String name;

  private VerificationStrategy(int id, String name) {
    this.id = id;
    this.name = name;
  }
  
  public String toString() {
    return name;
  }
  
  
  public static VerificationStrategy fromId(String id) {
	return VerificationStrategy.values()[Integer.parseInt(id)-1];
  }
  
  public static Map<String, String> options() {
    LinkedHashMap<String, String> options = new LinkedHashMap<String, String>();
    for (VerificationStrategy verificationStrategy : VerificationStrategy.values()) {
        options.put(verificationStrategy.id+"",verificationStrategy.name);
    }
    return options;
}
}
