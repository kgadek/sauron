package models;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import play.db.ebean.Model;

@Entity
public class Task extends Model {
    
  private static final long serialVersionUID = 4781377571624040428L;
  
  @Id
  public Long id;
  @Column(length=10000)
  public String code;
  public String name;
  public String description;
  
  public transient String taskTypeId;
  public TaskType taskType;

  public Double lon;
  public Double lat;
  public Double rad;
  public String deadline;
  public List<Sensor> sensors;
  
  @Column(length=10000)
  public String polygonPoints;
  public String getPolygon() {
    String temp = polygonPoints.replaceAll("\t", "");
    temp = temp.replaceAll(System.getProperty("line.separator"), "");
    return temp;
  }
  
  
  public transient String verificationStrategyId ;
  public VerificationStrategy verificationStrategy;
  public String verification;
  
  @OneToMany(cascade=CascadeType.ALL, mappedBy="task")
  @Column(name="RESULT")
  private Set<Result> results;
  
  public Set<Result> getResults() {
    return results;
  }
    @Column(length=10000)
  public String input;
  @Column(length=10000)
  public String mapCode;
  @Column(length=10000)
  public String reduceCode;
  @OneToMany(cascade=CascadeType.ALL, mappedBy="task")
  public Set<Subtask> subtasks;
  
  public String getSubtaskResult() {
    StringBuilder reduce = new StringBuilder("inputs for reduce : [ ");
    StringBuilder map = new StringBuilder("inputs for map : [ ");
    
    for (Subtask subtask : subtasks) {
      if(subtask.isMerge && !subtask.resolved) {
        reduce.append("[ ").append(subtask.val1).append(" , ").append(subtask.val2).append(" ],");
      } else if (!subtask.resolved){
        map.append(subtask.val1).append(",");
      }
    }
    reduce.append(" ]");
    map.append(" ] ");
    if(reduce.lastIndexOf(",") != -1) {
      reduce.deleteCharAt(reduce.lastIndexOf(","));
    } if(map.lastIndexOf(",") != -1) {
      map.deleteCharAt(map.lastIndexOf(",")).append("\n");
    }
    return map.append(reduce).toString();
  }
  
  public Long subtask_id;
  
  public static List<Task> all() {
    return find.all();
  }

  public static void create(Task task) {
    task.uglyEnumHack();
    if(TaskType.MAP_REDUCE.equals(task.taskType)) {
      task.uglyMapReduceBreakdown();
    }
    task.save();
  }

  public void uglyEnumHack() {
    verificationStrategy = VerificationStrategy.fromId(verificationStrategyId);
    taskType = TaskType.fromId(taskTypeId);
  }
  
  public void uglyMapReduceBreakdown() {
    subtasks = new HashSet<Subtask>();
    String tempInput[] = input.replaceAll("\\[", "").replaceAll("\\]", "").split(",");
    for(int i = 0; i < tempInput.length-2; i+=3) {
      String input = "[" + tempInput[i] + "," + tempInput[i+1] + "," + tempInput[i+2] + "]";
      subtasks.add(new Subtask(false, input, "", mapCode, this));
    }
    if(tempInput.length % 3 == 2) {
      String input = "[" + tempInput[tempInput.length-2] + "," + tempInput[tempInput.length-1] + "]";
      subtasks.add(new Subtask(false, input, "", mapCode, this));
    } else if(tempInput.length % 3 == 1) {
      String input = "[" + tempInput[tempInput.length-1] + "]";
      subtasks.add(new Subtask(false, input, "", mapCode, this));
    }
  }
  
  public Subtask getSubtask() {
    refresh();
    for (Subtask subtask : subtasks) {
      if(!subtask.isMerge && !subtask.resolved) {
        return subtask;
      }
    }
    for (Subtask subtask : subtasks) {
      if(subtask.isMerge && !subtask.val2.isEmpty() && !subtask.resolved) {
        return subtask;
      }
    }
    return null;// won't happen ?
  }
  
  public void handleSubtask(long subtaskId, String result, String time, double lon, double lat) {
    Subtask subtask = findSubtask(subtaskId);
    subtask.resolved = true;
    subtask.update();
    copyResultToMergeSubtask(result);
    if(isAfterLastMerge()) {
      appendResult(result, time, lon, lat);
      subtasks = null;
    }
  }

  private boolean isAfterLastMerge() {
    boolean last = false;
    for (Subtask subtask : subtasks) {
      if(!subtask.resolved && !last) {
        if(subtask.val2.isEmpty()) {
          last = true;
        } else {
          return false;
        }
      } else if(!subtask.resolved) {
        return false;
      }
    }
    return true;
  }
  
  private Subtask findSubtask(long subtaskId) {
    for (Subtask subtask : subtasks) {
      if(subtask.subtaskId == subtaskId)
        return subtask;
    }
    return null;
  }
  
  private void copyResultToMergeSubtask(String result) {
    for(Subtask subtask : subtasks) {
      if(subtask.isMerge && (subtask.val2 == null || subtask.val2.isEmpty())) {
        subtask.setVal2(result);
        subtask.save();
        return;
      }
    }
    subtasks.add(new Subtask(true, result, "", reduceCode, this));
  }
  
  public void appendResult(String result, String resultTime, double resultLon, double resultLat) {
    if(results == null) {
      results = new HashSet<Result>();
    }
    results.add(new Result(result, resultLon, resultLat, resultTime, this));
  }
  
  public String lastTime() {
    return results.iterator().next().resultTime;
  }
  
  public double lastLon() {
    return results.iterator().next().resultLon;
  }

  public double lastLat() {
    return results.iterator().next().resultLat;
  }
  
  public String resultsToString() {
    if(results.size() != 1) {
      StringBuilder result = new StringBuilder();
      result.append("[");
      for (Result resultA : results) {
        result.append(resultA.result).append(",");
      }
      result.deleteCharAt(result.lastIndexOf(",")).append("]");
      return result.toString();
    } else {
      return results.iterator().next().result;
    }
  }
  
  public boolean validateResult(String result) {
    if (verificationStrategy.equals(VerificationStrategy.NO_VERIFICATION)) {
      return true;
    } else if(verificationStrategy.equals(VerificationStrategy.VERIFICATION_BY_CODE)) {
//      Lua
    } else if(verificationStrategy.equals(VerificationStrategy.VERIFICATION_BY_MAJORITY)) {
//      Majority
    }
    return true;
  }
  
  public static void delete(Long id) {
    find.ref(id).delete();
  }
  public static Finder<Long,Task> find = new Finder<Long, Task>(
      Long.class, Task.class
      );

}
