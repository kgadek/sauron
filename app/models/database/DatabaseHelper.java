package models.database;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import models.database.model.*;

import java.sql.SQLException;

/**
 * Created by banan on 21.01.14.
 */
public class DatabaseHelper {

    private Class[] TABLES = {DelegatedTask.class, RequiredSensor.class, Result.class,
            Sensor.class, SpecialTask.class, SpecialTaskType.class, Subtask.class, Task.class,
            VerificationStrategy.class};

    private static ConnectionSource connectionSource;

    public DatabaseHelper() {
        try {
            connectionSource = new JdbcConnectionSource("jdbc:sqlite:./database");
            for (Class clazz : TABLES)
                TableUtils.createTableIfNotExists(connectionSource, clazz);

            // init data
            if (getSpecialTaskTypeDao().queryForAll().size() == 0) {
                SpecialTaskType type = new SpecialTaskType();
                type.setName("PHOTO");
                getSpecialTaskTypeDao().create(type);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * daos
     */
    private Dao<DelegatedTask, Long> delegatedTaskDao;
    public Dao<DelegatedTask, Long> getDelegatedTaskDao() throws SQLException {
        if (delegatedTaskDao == null)
            delegatedTaskDao =
                    DaoManager.createDao(connectionSource, DelegatedTask.class);
        return delegatedTaskDao;
    }

    private Dao<RequiredSensor, Long> requiredSensorDao;
    public Dao<RequiredSensor, Long> getRequiredSensorDao() throws SQLException {
        if (requiredSensorDao == null)
            requiredSensorDao =
                    DaoManager.createDao(connectionSource, RequiredSensor.class);
        return requiredSensorDao;
    }

    private Dao<Result, Long> resultDao;
    public Dao<Result, Long> getResultDao() throws SQLException {
        if (resultDao == null)
            resultDao =
                    DaoManager.createDao(connectionSource, Result.class);
        return resultDao;
    }

    private Dao<Sensor, Long> sensorDao;
    public Dao<Sensor, Long> getSensorDao() throws SQLException {
        if (sensorDao == null)
            sensorDao =
                    DaoManager.createDao(connectionSource, Sensor.class);
        return sensorDao;
    }

    private Dao<SpecialTask, Long> specialTaskDao;
    public Dao<SpecialTask, Long> getSpecialTaskDao() throws SQLException {
        if (specialTaskDao == null)
            specialTaskDao =
                    DaoManager.createDao(connectionSource, SpecialTask.class);
        return specialTaskDao;
    }

    private Dao<SpecialTaskType, Long> specialTaskTypeDao;
    public Dao<SpecialTaskType, Long> getSpecialTaskTypeDao() throws SQLException {
        if (specialTaskTypeDao == null)
            specialTaskTypeDao =
                    DaoManager.createDao(connectionSource, SpecialTaskType.class);
        return specialTaskTypeDao;
    }

    private Dao<Subtask, Long> subtaskDao;
    public Dao<Subtask, Long> getSubtaskDao() throws SQLException {
        if (subtaskDao == null)
            subtaskDao =
                    DaoManager.createDao(connectionSource, Subtask.class);
        return subtaskDao;
    }

    private Dao<Task, Long> taskDao;
    public Dao<Task, Long> getTaskDao() throws SQLException {
        if (taskDao == null)
            taskDao =
                    DaoManager.createDao(connectionSource, Task.class);
        return taskDao;
    }

    private Dao<VerificationStrategy, Long> verificationStrategyDao;
    public Dao<VerificationStrategy, Long> getVerificationStrategyDao() throws SQLException {
        if (verificationStrategyDao == null)
            verificationStrategyDao =
                    DaoManager.createDao(connectionSource, VerificationStrategy.class);
        return verificationStrategyDao;
    }

}
