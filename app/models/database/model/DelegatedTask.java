package models.database.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by banan on 21.01.14.
 */
@DatabaseTable(tableName = DelegatedTask.TABLE_NAME)
public class DelegatedTask {

    public static final String TABLE_NAME = "delegated_tasks";

    public class TableColumns {
        public static final String ID = "id";
        public static final String SUBTASK_ID = "subtask_id";
        public static final String DATE = "date";
    }


    /**
     * DB FIELDS
     */
    @DatabaseField(generatedId = true, columnName = TableColumns.ID)
    @SerializedName("id") private Long id;

    @DatabaseField(columnName = TableColumns.SUBTASK_ID)
    @SerializedName("subtask_id") private Long subtaskId;

    @DatabaseField(columnName = TableColumns.DATE)
    @SerializedName("date") private Long date;

    public DelegatedTask() {;}


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSubtaskId() {
        return subtaskId;
    }

    public void setSubtaskId(Long subtaskId) {
        this.subtaskId = subtaskId;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }
}
