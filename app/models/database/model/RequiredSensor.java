package models.database.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by banan on 21.01.14.
 */

@DatabaseTable(tableName = RequiredSensor.TABLE_NAME)
public class RequiredSensor {

    public static final String TABLE_NAME = "required_sensors";

    public class TableColumns {
        public static final String ID = "id";
        public static final String SENSOR_ID = "sensor_id";
        public static final String SUBTASK_ID = "subtask_id";
    }


    /**
     * DB FIELDS
     */

    @DatabaseField(generatedId = true, columnName = TableColumns.ID)
    @SerializedName("sensor_id") private Long id;

    @DatabaseField(columnName = TableColumns.SENSOR_ID)
    @SerializedName("sensor_id") private Long sensorId;

    @DatabaseField(columnName = TableColumns.SUBTASK_ID)
    @SerializedName("subtask_id") private Long subtaskId;

    public RequiredSensor() { ;}


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSensorId() {
        return sensorId;
    }

    public void setSensorId(Long sensorId) {
        this.sensorId = sensorId;
    }

    public Long getSubtaskId() {
        return subtaskId;
    }

    public void setSubtaskId(Long subtaskId) {
        this.subtaskId = subtaskId;
    }
}
