package models.database.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by banan on 21.01.14.
 */
@DatabaseTable(tableName = Sensor.TABLE_NAME)

public class Sensor {

    public static final String TABLE_NAME = "sensors";

    public class TableColumns {
        public static final String ID = "id";
        public static final String NAME = "name";
    }

    /**
     * DB FIELDS
     */
    @DatabaseField(generatedId = true, columnName = TableColumns.ID)
    @SerializedName("id") private Long id;

    @DatabaseField(columnName = TableColumns.NAME)
    @SerializedName("name") private String name;

    public Sensor() { ;}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
