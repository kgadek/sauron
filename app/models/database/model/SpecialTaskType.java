package models.database.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by banan on 21.01.14.
 */
@DatabaseTable(tableName = SpecialTaskType.TABLE_NAME)
public class SpecialTaskType {

    public static final String TABLE_NAME = "special_task_type";

    public class TableColumns {
        public static final String ID = "id";
        public static final String NAME = "name";
    }

    /**
     * DB FIELDS
     */

    @DatabaseField(generatedId = true, columnName = SpecialTaskType.TableColumns.ID)
    @SerializedName("id") private Long id;

    @DatabaseField(columnName = SpecialTaskType.TableColumns.NAME)
    @SerializedName("name") private String name;

    public SpecialTaskType() { ;}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
