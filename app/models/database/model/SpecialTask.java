package models.database.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by banan on 21.01.14.
 */
@DatabaseTable(tableName = SpecialTask.TABLE_NAME)
public class SpecialTask {

    public static final String TABLE_NAME = "special_tasks";

    public class TableColumns {
        public static final String SUBTASK_ID = "subtask_id";
        public static final String SPECIAL_TASK_TYPE_ID = "special_task_type_id";
        public static final String NAME = "name";
        public static final String DESCRIPTION = "description";
    }


    /**
     * DB FIELDS
     */
    @DatabaseField(id = true, columnName = TableColumns.SUBTASK_ID)
    @SerializedName("subtask_id") private Long subtaskId;

    @DatabaseField(columnName = TableColumns.SPECIAL_TASK_TYPE_ID)
    @SerializedName("special_task_type") private Long specialTaskTypeId;

    @DatabaseField(columnName = TableColumns.NAME)
    @SerializedName("name") private String name;

    @DatabaseField(columnName = TableColumns.DESCRIPTION)
    @SerializedName("description") private String description;

    public SpecialTask() {;}

    public Long getSubtaskId() {
        return subtaskId;
    }

    public void setSubtaskId(Long subtaskId) {
        this.subtaskId = subtaskId;
    }

    public Long getSpecialTaskTypeId() {
        return specialTaskTypeId;
    }

    public void setSpecialTaskTypeId(Long specialTaskTypeId) {
        this.specialTaskTypeId = specialTaskTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
