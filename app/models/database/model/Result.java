package models.database.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by banan on 21.01.14.
 */
@DatabaseTable(tableName = Result.TABLE_NAME)
public class Result {

    public static final String TABLE_NAME = "results";

    public class TableColumns {
        public static final String ID = "id";
        public static final String DELEGATED_SUBTASK_ID = "delegated_subtask_id";
        public static final String RESULT = "result";
        public static final String DATE = "date";
        public static final String LAT = "lat";
        public static final String LON = "lon";
    }

    @DatabaseField(generatedId = true, columnName = TableColumns.ID)
    @SerializedName("id") private Long id;

    @DatabaseField(columnName = TableColumns.DELEGATED_SUBTASK_ID)
    @SerializedName("delegated_subtask_id") private Long delegatedSubtaskId;

    @DatabaseField(columnName = TableColumns.RESULT)
    @SerializedName("result") private String result;

    @DatabaseField(columnName = TableColumns.DATE)
    @SerializedName("date") private Long date;

    @DatabaseField(columnName = TableColumns.LAT)
    @SerializedName("lat") private Double lat;

    @DatabaseField(columnName = TableColumns.LON)
    @SerializedName("lon") private Double lon;

    public Result() {;}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDelegatedSubtaskId() {
        return delegatedSubtaskId;
    }

    public void setDelegatedSubtaskId(Long delegatedSubtaskId) {
        this.delegatedSubtaskId = delegatedSubtaskId;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }
}
