package models.database.model;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by banan on 21.01.14.
 */
@DatabaseTable(tableName = Subtask.TABLE_NAME)
public class Subtask {

    public static final String TABLE_NAME = "subtasks";

    public class TableColumns {
        public static final String ID = "id";
        public static final String TASK_ID = "task_id";
        public static final String RUN_COUNT = "run_count";
        public static final String LAT = "lat";
        public static final String LON = "lon";
        public static final String RAD = "rad";
        public static final String VALID_UNTIL = "valid_until";
        public static final String LUA_CODE = "lua_code";
        public static final String VERIFICATION_CODE = "verification_code";
        public static final String VERIFICATION_STRATEGY_ID = "verification_strategy_id";
        public static final String BACKGROUND_TASK = "background_task";
    }


    /**
     * DB FIELDS
     */
    @DatabaseField(generatedId = true, columnName = Subtask.TableColumns.ID)
    @SerializedName("id") private Long id;

    @DatabaseField(columnName = TableColumns.TASK_ID)
    @SerializedName("task_id") private Long taskId;

    @DatabaseField(columnName = TableColumns.RUN_COUNT)
    @SerializedName("run_count") private int runCount;

    @DatabaseField(columnName = TableColumns.LAT)
    @SerializedName("lat") private Double lat;

    @DatabaseField(columnName = TableColumns.LON)
    @SerializedName("lon") private Double lon;

    @DatabaseField(columnName = TableColumns.RAD)
    @SerializedName("rad") private int rad;

    @DatabaseField(columnName = TableColumns.VALID_UNTIL)
    @SerializedName("valid_until") private Long validUntil;

    @DatabaseField(columnName = TableColumns.LUA_CODE)
    @SerializedName("lua_code") private String luaCode;

    @DatabaseField(columnName = TableColumns.VERIFICATION_CODE)
    @SerializedName("verification_code") private String verificationCode;

    @DatabaseField(columnName = TableColumns.VERIFICATION_STRATEGY_ID)
    @SerializedName("verification_strategy_id") private Long verificationStrategyId;


    @DatabaseField(columnName = TableColumns.BACKGROUND_TASK)
    @SerializedName("background_task") private boolean backgroundTask;


    /**
     * non db fields
     */

    private Task task;


    public Subtask() { ;}


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public int getRunCount() {
        return runCount;
    }

    public void setRunCount(int runCount) {
        this.runCount = runCount;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public int getRad() {
        return rad;
    }

    public void setRad(int rad) {
        this.rad = rad;
    }

    public Long getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Long validUntil) {
        this.validUntil = validUntil;
    }

    public String getLuaCode() {
        return luaCode;
    }

    public void setLuaCode(String luaCode) {
        this.luaCode = luaCode;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public Long getVerificationStrategyId() {
        return verificationStrategyId;
    }

    public void setVerificationStrategyId(Long verificationStrategyId) {
        this.verificationStrategyId = verificationStrategyId;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public boolean isBackgroundTask() {
        return backgroundTask;
    }

    public void setBackgroundTask(boolean backgroundTask) {
        this.backgroundTask = backgroundTask;
    }
}
