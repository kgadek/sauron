package controllers;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

import models.Sensor;
import models.Task;
import models.TaskType;
import models.VerificationStrategy;
import models.database.DatabaseHelper;
import models.database.model.Subtask;
import play.Logger;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import scheduler.Scheduler;
import scheduler.SimpleScheduler;
import sun.misc.BASE64Decoder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.mvc.*;
import play.data.*;
import static play.data.Form.*;

import views.html.*;


public class Application extends Controller {

  static Form<Task> taskForm = Form.form(Task.class);
  static Form<Subtask> subtaskForm = Form.form(Subtask.class);
  static Scheduler scheduler = new SimpleScheduler();
  static DatabaseHelper databaseHelper = new DatabaseHelper();
  
 
public static Result index() {
    Logger.debug("index");
    return ok(
        index.render(Task.find.query().order().asc("id").findList(), taskForm, subtaskForm)
        );
  }
  
  public static Result getTaskPage(long id) {
	return ok(
			taskSite.render(Task.find.byId(id), taskForm, subtaskForm)
	);

  }
  
  public static Result getCreatePage(String id) {
	return ok(
			createTask.render(id, taskForm, subtaskForm)
	);

  }

  public static Result submit() {
  Form<Task> filledForm = taskForm.bindFromRequest();
  Logger.info(filledForm.field("taskTypeId").value());
  String id = filledForm.field("taskTypeId").value();
	
	return ok(createTask.render(id, taskForm, subtaskForm));
  }
  
  
  public static Result getTasks() {
    Logger.debug("getTask");
    JsonNode requestJson = request().body().asJson();
    List<Sensor> sensors = new LinkedList<Sensor>();
    Iterator<JsonNode> it = requestJson.get("sensors").iterator();
    while(it.hasNext()) {
      sensors.add(Sensor.fromId(it.next().asText()));
    }
    Task task = scheduler.getTask(requestJson.get("lat").doubleValue(), requestJson.get("lon").doubleValue(), sensors);
    if(task != null) {
      task.refresh();
    }
    ObjectNode result = Json.newObject();
    ArrayNode background_tasks = result.putArray("background_tasks");
    ArrayNode special_tasks = result.putArray("special_tasks");
    if(task != null) {
      encodeTaskForClient(task, background_tasks, special_tasks);
    }
    return ok(result);
  }

  private static void encodeTaskForClient(Task task, ArrayNode background_tasks,
      ArrayNode special_tasks) {
    ObjectNode taskNode = Json.newObject();
    taskNode.put("id", task.id);
    if(task.taskType.isBackgroundTask()) {
      taskNode.put("code", task.code);
      background_tasks.add(taskNode);
    } else if(task.taskType.equals(TaskType.MAP_REDUCE)) {
      background_tasks.add(Task.find.byId(task.id).getSubtask().asJson()); // Seems like ebean have some bugs in refreshing dirty entity... 
    }else {
      taskNode.put("type", task.taskType.id);
      taskNode.put("lat", task.lat);
      taskNode.put("lon", task.lon);
      taskNode.put("rad", task.rad);
      taskNode.put("deadline", task.deadline);
      taskNode.put("name", task.name);
      taskNode.put("description", task.description);
      special_tasks.add(taskNode);
    }
  }
  
  public static Result receiveResult() throws IOException {
	Logger.debug("receive result");
    JsonNode requestJson = request().body().asJson();
    Task task = Task.find.byId(requestJson.get("id").asLong());
    if(task.validateResult(requestJson.get("result").asText())) {
      if(task.taskType.equals(TaskType.MAP_REDUCE)) {
        task.refresh();
        task.handleSubtask(requestJson.get("subtask_id").asLong(), requestJson.get("result").asText(),requestJson.get("time").asText(), requestJson.get("lon").asDouble(), requestJson.get("lat").asDouble());
        if(task.subtasks == null || task.subtasks.isEmpty()) {
          scheduler.removeTask(task);
        }
      } else {
        task.appendResult(requestJson.get("result").asText(),requestJson.get("time").asText(), requestJson.get("lon").asDouble(), requestJson.get("lat").asDouble());
        scheduler.removeTask(task);
        if(task.taskType != null && task.taskType.equals(TaskType.PHOTO)) {
          decodePhoto(task);
        }
      }
      task.update();
    }
    return ok();
  }

  private static void decodePhoto(Task task) throws IOException {
    task.getResults().iterator().next().result = task.getResults().iterator().next().result.replaceAll("\n", "");
    task.getResults().iterator().next().result = task.getResults().iterator().next().result.replaceAll("_", "/");
    task.getResults().iterator().next().result = task.getResults().iterator().next().result.replaceAll("-", "+");
    BufferedImage image = decodeToImage(task.getResults().iterator().next().result);
    File outputfile = new File("image-"+task.id + ".png");
    ImageIO.write(image, "png", outputfile);
  }

  public static BufferedImage decodeToImage(String imageString) {

    BufferedImage image = null;
    byte[] imageByte;
    try {
        BASE64Decoder decoder = new BASE64Decoder();
        imageByte = decoder.decodeBuffer(imageString);
        ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
        image = ImageIO.read(bis);
        bis.close();
    } catch (Exception e) {
        e.printStackTrace();
    }
    return image;
}
  
  public static Result addNewTask() {
    JsonNode requestJson = request().body().asJson();
    ObjectNode result= Json.newObject();
    ArrayNode ids = result.putArray("ids");
    if(requestJson.has("background_tasks")) {
      addBackgroundTasks(requestJson, ids);
    }
    if(requestJson.has("special_tasks")) {
      addSpecialTasks(requestJson, ids);
    }
    return ok(result);
  }

  private static void addSpecialTasks(JsonNode requestJson, ArrayNode ids) {
    Iterator<JsonNode> it = requestJson.get("special_tasks").iterator();
    while(it.hasNext()) {
      JsonNode taskNode = it.next();
      Task task = new Task();
      task.taskType = TaskType.fromId(taskNode.get("type").asText());
      task.lat = taskNode.get("lat").asDouble();
      task.lon = taskNode.get("lon").asDouble();
      task.rad = taskNode.get("rad").asDouble();
      task.deadline = taskNode.get("deadline").asText();
      task.name = taskNode.get("name").asText();
      task.description = taskNode.get("description").asText();
      if(taskNode.has("task_id")) {
        task.id = taskNode.get("task_id").asLong();
      }
      Task.create(task);
      ids.add(task.id);
    }
  }

  private static void addBackgroundTasks(JsonNode requestJson, ArrayNode ids) {
    Iterator<JsonNode> it = requestJson.get("background_tasks").iterator();
    while(it.hasNext()) {
      JsonNode taskNode = it.next();
      Task task = new Task();
      task.code = requestJson.get("code").asText();
      task.taskType = TaskType.BACKGROUND_TASK;
      task.verificationStrategyId = taskNode.get("verification_strategy").textValue();
      if (task.verificationStrategyId == null) {
      	task.verificationStrategyId = "a";
      }
      if(taskNode.has("task_id")) {
        task.id = taskNode.get("task_id").asLong();
      }
      if(taskNode.has("sensors")) {
        Iterator<JsonNode> senIt = taskNode.get("sensors").iterator();
        List<Sensor> sensors = new LinkedList<Sensor>();
        while(senIt.hasNext()) {
          sensors.add(Sensor.fromId(senIt.next().asText()));
        }
        task.sensors = sensors;
      }
      if(task.verificationStrategy.equals(VerificationStrategy.VERIFICATION_BY_CODE)) {
        task.verification = taskNode.get("code_verify").textValue();
      } 
      Task.create(task);
      ids.add(task.id);
      scheduler.scheduleTask(task);
    }
  }
  
  public static Result getResult(Long id) {
    Task task = Task.find.byId(id);
    ObjectNode result = Json.newObject();
    result.put("id", task.id);
    ArrayNode resultList = result.putArray("result");
    for (models.Result stringResult : task.getResults()) {
      resultList.add(stringResult.result);
    }
    if(task.getResults().iterator().hasNext()) {
      result.put("time", task.lastTime());
      result.put("lat", task.lastLat());
      result.put("lon", task.lastLon());
    }
    return ok(result);
  }
  
  public static Result deleteTask(Long id) {
    scheduler.removeTask(Task.find.byId(id));
    Task.delete(id);
    return redirect(routes.Application.index());
  }
  
  ////////////////////////
  
  public static Result newTask() {
    Form<Task> filledForm = taskForm.bindFromRequest();
    Task.create(filledForm.get());
    scheduler.scheduleTask(filledForm.get());
    return redirect(routes.Application.index());
  }

//  public static Result newSubtask() {
//    Form<Subtask> filledForm = subtaskForm.bindFromRequest();
//    Subtask subtask = filledForm.get();
//    return redirect(routes.Application.index());
//  }
//  
//  public static Map<String, String> getTasksId() throws SQLException {
//    LinkedHashMap<String, String> options = new LinkedHashMap<String, String>();
//    for (models.database.model.Task task : databaseHelper.getTaskDao().queryForAll()) {
//        options.put(task.getId()+"",task.getName());
//    }
//    return options;
//}
  
}
