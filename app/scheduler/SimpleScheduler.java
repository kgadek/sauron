package scheduler;

import java.awt.Polygon;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import play.libs.Json;

import models.Sensor;
import models.Task;


public class SimpleScheduler implements Scheduler {

  private List<Task> tasks = Task.find.findList();
  private DistanceCalculator distanceCalculator = new DistanceCalculator();
  @Override
  public void scheduleTask(Task task) {
    tasks.add(task);
  }
  
  @Override
  public void removeTask(Task task) {
    tasks.remove(task);
  }
  
  @Override
  public Task getTask() {
    return tasks.get(0);
  }

  private boolean distanceCheck(Task task, double lat, double lon) {
    if(task.polygonPoints != null && !task.polygonPoints.isEmpty()) {
      JsonNode polygonPointsJson = Json.parse(task.polygonPoints);
      ArrayNode pointsArray = (ArrayNode) polygonPointsJson.get("points");
      Iterator<JsonNode> it = pointsArray.iterator();
      Polygon polygon = new Polygon();
      while(it.hasNext()) {
        JsonNode point = it.next();
        polygon.addPoint(point.get("x").asInt(), point.get("y").asInt());
      }
      return polygon.contains(lat*1000000, lon*1000000);
    } else if(task.rad != null && task.rad != -1) {
      double distance = distanceCalculator.distance(lat, lon, task.lat, task.lon);
      return distance < task.rad; 
    }
    return true;
  }
  
  private boolean sensorCheck(Task task, List<Sensor> sensor) {
    if(sensor != null && task.sensors != null) {
      return sensor.containsAll(task.sensors);
    }
    return true;
  }
  
  private boolean deadlineCheck(Task task) {
    if(task.deadline != null && !task.deadline.isEmpty()) {
      return new Date().before(new Date(Long.parseLong(task.deadline)));
    }
    return true;
  }
  
  @Override
  public Task getTask(double lat, double lon, List<Sensor> sensor) {
    Iterator<Task> it = tasks.iterator();
    while(it.hasNext()) {
      Task task = it.next();
      if(task.getResults() != null &&!task.getResults().isEmpty()) {
        tasks.remove(task);
      } else if(distanceCheck(task, lat, lon) && sensorCheck(task, sensor) && deadlineCheck(task)) {
        tasks.remove(task);
        tasks.add(task);
        return task;
      }
    }
    return null;
  }

}
