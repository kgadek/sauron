package scheduler;

import java.util.List;

import models.Sensor;
import models.Task;


public interface Scheduler {
  public void scheduleTask(Task task);
  public void removeTask(Task task);
  public Task getTask();
  public Task getTask(double lat, double lon, List<Sensor> sensor);
}
