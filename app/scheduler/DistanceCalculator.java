package scheduler;

public class DistanceCalculator {
  private static final Double EARTH_RADIUS = new Double(6371); // km

  public double distance(double lat1, double lon1, double lat2, double lon2) {
    double dLat = (lat2-lat1) * Math.PI / 180;
    double dLon = (lon2-lon1) * Math.PI / 180;
    lat1 = lat1 * Math.PI / 180;
    lat2 = lat2 * Math.PI / 180;

    double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    double d = EARTH_RADIUS * c;
    return d;
  }
}
