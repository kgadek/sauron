-- =====================================================================================================================
-- TEGO NIE POWINNO BYĆ W APPCE WYSYŁANEJ NA ANDROIDA!
-- =====================================================================================================================

-- Służy tylko do zamockowania interfejsu Androida - do testowania na komputerze.
if not android then
    android = require "android_iface_mock"
end


-- ---------------------------------------------------------------------------------------------------------------------
-- uruchomienie zadania
-- ---------------------------------------------------------------------------------------------------------------------
function fib(x) return x<2 and x or fib(x-1)+fib(x-2) end

local n = 10

for i=0,n do
    local res = fib(i)
    android:send(tostring(res))
end
