-- =====================================================================================================================
-- MOCK INTERFEJSU DOSTĘPNEGO NA APPCE
-- Dzięki temu można "ot tak" wrzucić kod w interpreter i będzie działać
-- ---------------------------------------------------------------------------------------------------------------------
local android = {}
function android.send(self, x)
    print("ANDROID:SEND:", x)
end
function android.sleep(self, t)
    print("ANDROID:SLEEP:", t)
    local os = require "os"
    os.execute("sleep "..tonumber(t))
end

local gps_latitude = 20.019
function android.getLatitude(self)
    local res = gps_latitude
    gps_latitude = gps_latitude - 0.01
    print("ANDROID:GETLATITUDE >>", res)
    return res
end

local gps_longitude = 50.0655
function android.getLongitude(self)
    local res = gps_longitude
    gps_longitude = gps_longitude + 0.01
    print("ANDROID:GETLLONGITUDE >>", res)
    return res
end
function android.getTime(self)
    local res = os.time()
    print("ANDROID:GETLLONGITUDE >>", res)
    return res
end
return android