-- =====================================================================================================================
-- TEGO NIE POWINNO BYĆ W APPCE WYSYŁANEJ NA ANDROIDA!
-- =====================================================================================================================

-- Służy tylko do zamockowania interfejsu Androida - do testowania na komputerze.
if not android then
    android = require "android_iface_mock"
end

-- Doklejana zawartość
local val1 = {0, 2, 2, 4, 6}
local val2 = {0, 1, 2, 3, 4}


-- =====================================================================================================================
-- ===== MERGE SORT (REDUCE) ===========================================================================================
-- =====================================================================================================================


-- ===== CHUNKIFY-MAP-REDUCE ===========================================================================================
function reduce(xs, ys)
    local res = {}
    while next(xs) ~= nil and next(ys) ~= nil do
        if xs[1] <= ys[1]
        then table.insert(res, table.remove(xs, 1))
        else table.insert(res, table.remove(ys, 1)) end
    end
    if next(xs) == nil then
        for k,v in pairs(ys) do table.insert(res,v) end
    elseif next(ys) == nil then 
        for k,v in pairs(xs) do table.insert(res,v) end
    end
    return res
end


-- ===== helpers =======================================================================================================
function table2json(xs)
    return "[" .. table.concat(xs, ',') .. "]"
end

android:send(table2json(reduce(val1, val2)))