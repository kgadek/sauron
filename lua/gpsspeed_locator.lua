-- =====================================================================================================================
-- TEGO NIE POWINNO BYĆ W APPCE WYSYŁANEJ NA ANDROIDA!
-- =====================================================================================================================

-- Służy tylko do zamockowania interfejsu Androida - do testowania na komputerze.
if not android then
    android = require "android_iface_mock"
end


-- =====================================================================================================================
-- ===== GPS SPEED =====================================================================================================
-- =====================================================================================================================

math = require "math"

function degrees_to_radians(deg)
    return deg * math.pi / 180.0
end

function coordinates_to_distance(lat1, lon1, lat2, lon2)
    R = 6378137
    dLat = degrees_to_radians(lat2-lat1)
    dLon = degrees_to_radians(lon2-lon1)
    lat1 = degrees_to_radians(lat1)
    lat2 = degrees_to_radians(lat2)

    a = math.sin(dLat/2) * math.sin(dLat/2) + math.sin(dLon/2) * math.sin(dLon/2) * math.cos(lat1) * math.cos(lat2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = R * c
    return d
end

local lat1 = android:getLatitude()
local lon1 = android:getLongitude()
android:sleep(1)
local lat2 = android:getLatitude()
local lon2 = android:getLongitude()

android:send(coordinates_to_distance(lat1, lon1, lat2, lon2))  -- rezultat w m/s
-- android:send(coordinates_to_distance(lat1, lon1, lat2, lon2) * 3.6)  -- rezultat w km/h