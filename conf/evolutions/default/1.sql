# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table result (
  id                        bigint not null,
  RESULT                    varchar(100000),
  result_lon                double,
  result_lat                double,
  result_time               varchar(255),
  task_id                   bigint not null,
  constraint pk_result primary key (id))
;

create table subtask (
  subtask_id                bigint not null,
  resolved                  boolean,
  is_merge                  boolean,
  val1                      varchar(255),
  val2                      varchar(255),
  code                      varchar(10000),
  task_id                   bigint not null,
  constraint pk_subtask primary key (subtask_id))
;

create table task (
  id                        bigint not null,
  code                      varchar(10000),
  name                      varchar(255),
  description               varchar(255),
  task_type                 integer,
  lon                       double,
  lat                       double,
  rad                       double,
  deadline                  varchar(255),
  polygon_points            varchar(10000),
  verification_strategy     integer,
  verification              varchar(255),
  input                     varchar(10000),
  map_code                  varchar(10000),
  reduce_code               varchar(10000),
  subtask_id                bigint,
  constraint ck_task_task_type check (task_type in (0,1,2)),
  constraint ck_task_verification_strategy check (verification_strategy in (0,1,2)),
  constraint pk_task primary key (id))
;

create sequence result_seq;

create sequence subtask_seq;

create sequence task_seq;

alter table result add constraint fk_result_task_1 foreign key (task_id) references task (id) on delete restrict on update restrict;
create index ix_result_task_1 on result (task_id);
alter table subtask add constraint fk_subtask_task_2 foreign key (task_id) references task (id) on delete restrict on update restrict;
create index ix_subtask_task_2 on subtask (task_id);



# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists result;

drop table if exists subtask;

drop table if exists task;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists result_seq;

drop sequence if exists subtask_seq;

drop sequence if exists task_seq;

